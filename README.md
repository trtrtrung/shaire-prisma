This project is used to generate DBML file for project **SHAIRE** only.

**TOOLS**

- [Prisma.](https://www.prisma.io/docs/reference)

**STEPS**

- Install project dependencies `yarn`.
- Replace `DATABASE_URL` with your database URL in `.env` file (make sure it is a mongodb database).
- Run `yarn prisma db pull` to update current db data structure to prisma.
- Run `yarn prisma generate` to generate a `schema.dbml` file located in `./prisma/dbml/schema.dbml`.
- Edit table, field name, data types,... in `schema.dbml`
- Build `schema.dbml` file with [dbdocs](https://dbdocs.io/docs).

Full command:

```bash
yarn
yarn prisma db pull
yarn prisma generate
dbdocs build ./prisma/dbml/schema.dbml
```
